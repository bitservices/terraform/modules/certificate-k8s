<!---------------------------------------------------------------------------->

# certificate (k8s)

<!---------------------------------------------------------------------------->

## Description

Manage certificates and related resources on [Kubernetes] with [cert-manager].

<!---------------------------------------------------------------------------->

## Modules

* [certificate](certificate/README.md) - Uses [cert-manager] to provision a certificate given a DNS name and issuer.

<!---------------------------------------------------------------------------->

[Kubernetes]:   https://kubernetes.io/
[cert-manager]: https://cert-manager.io/

<!---------------------------------------------------------------------------->
