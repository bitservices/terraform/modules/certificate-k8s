###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The name for the Certificate Manager certificate."
}

variable "namespace" {
  type        = string
  description = "Namespace that this certificate should be deployed in to."
}

variable "dns_names" {
  type        = list(string)
  description = "A list of domain names that this certificate should be for. The first domain name in the list is used for the common name."
}

###############################################################################
# Optional Variables
###############################################################################

variable "kind" {
  type        = string
  default     = "Certificate"
  description = "Identifier kind for Certificate Manager certificates. This should never be overridden."
}

variable "labels" {
  type        = map(string)
  default     = {}
  description = "Additional labels to be assigned to the Certificate Manager certificate."
}

variable "name_label" {
  type        = string
  default     = "app.kubernetes.io/name"
  description = "The label key for the label holding the certificates name."
}

variable "api_version" {
  type        = string
  default     = "cert-manager.io/v1"
  description = "The Certificate Manager custom resource definition API version."
}

###############################################################################

variable "issuer_name" {
  type        = string
  default     = "lets-encrypt-live"
  description = "The Certificate Manager issuer name to use for this certificate."
}

variable "issuer_type" {
  type        = string
  default     = "ClusterIssuer"
  description = "The Certificate Manager issuer type that 'issuer_name' refers to."
}

###############################################################################
# Locals
###############################################################################

locals {
  common_name = var.dns_names[0]
}

###############################################################################
# Resources
###############################################################################

resource "kubernetes_manifest" "object" {
  manifest = {
    "apiVersion" = var.api_version
    "kind"       = var.kind

    "metadata" = {
      "name"      = var.name
      "labels"    = merge({ (var.name_label) = var.name }, var.labels)
      "namespace" = var.namespace
    }

    "spec" = {
      "dnsNames"   = var.dns_names
      "commonName" = local.common_name
      "secretName" = var.name

      "issuerRef" = {
        "kind" = var.issuer_type
        "name" = var.issuer_name
      }
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "dns_names" {
  value = var.dns_names
}

###############################################################################

output "name_label" {
  value = var.name_label
}

###############################################################################

output "issuer_name" {
  value = var.issuer_name
}

output "issuer_type" {
  value = var.issuer_type
}

###############################################################################

output "common_name" {
  value = local.common_name
}

###############################################################################

output "name" {
  value = kubernetes_manifest.object.manifest.metadata.name
}

output "kind" {
  value = kubernetes_manifest.object.manifest.kind
}

output "labels" {
  value = kubernetes_manifest.object.manifest.metadata.labels
}

output "namespace" {
  value = kubernetes_manifest.object.manifest.metadata.namespace
}

output "api_version" {
  value = kubernetes_manifest.object.manifest.apiVersion
}

###############################################################################
