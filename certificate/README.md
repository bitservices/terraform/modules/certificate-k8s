<!---------------------------------------------------------------------------->

# certificate

#### Uses [cert-manager] to provision a certificate given a DNS name and issuer

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/certificate/k8s//certificate`**

-------------------------------------------------------------------------------

### Example Usage

```
resource "kubernetes_namespace" "my_namespace" {
  metadata {
    name = "foobar"
  }
}

module "my_certificate" {
  source    = "gitlab.com/bitservices/certificate/k8s//certificate"
  name      = "foobar"
  namespace = kubernetes_namespace.my_namespace.metadata.0.name
  dns_names = [ "foo.bitservices.io", "bar.bitservices.io" ]
}
```

<!---------------------------------------------------------------------------->

[cert-manager]: https://cert-manager.io/

<!---------------------------------------------------------------------------->
